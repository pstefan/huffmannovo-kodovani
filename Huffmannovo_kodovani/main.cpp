#include <iostream>
#include <fstream>
#include <memory>
#include <queue>
#include <algorithm>
#include <string>
#include <array>

using namespace std;

#define PRINT


struct RNode;

typedef unique_ptr<array<unsigned long long, 256>> array_ptr;
typedef unique_ptr<RNode> huftree_ptr;
typedef unique_ptr<array<vector<bool>, 256>> codes_ptr;

struct RNode {
  char cSymbol;
  unsigned long long uFreq;
  bool fLeaf;
  huftree_ptr pLeft;
  huftree_ptr pRight;
  unsigned time;
  RNode(char cSym, unsigned long long uFreq, bool fLeaf, unsigned time = 0, huftree_ptr L = nullptr, huftree_ptr R = nullptr) :
    cSymbol(cSym), uFreq(uFreq), fLeaf(fLeaf), pLeft(move(L)), pRight(move(R)), time(time) {}
  RNode(istream& input);
  bool operator< (const RNode& right);
  void Save(ostream& ostr);
};

bool RNode::operator< (const RNode& right)
{
  if (this == nullptr)
    return true;
  else if (&right == nullptr)
    return false;
  else if (uFreq < right.uFreq)
    return true;
  else if (uFreq == right.uFreq) {
    if (fLeaf && !right.fLeaf)
      return true;
    else if (fLeaf && right.fLeaf && cSymbol < right.cSymbol)
      return true;
    else if (!fLeaf && !right.fLeaf && time < right.time)
      return true;
    else
      return false;
  }
  else
    return false;
}

void RNode::Save(ostream& ostr)
{
  char buffer[8] = {0};
  unsigned long long temp = uFreq;
  for (int i = 6; i >= 0; i--) {
    buffer[i] = static_cast<char>(temp);
    temp >>= 8;
  }
  if (fLeaf) {
    buffer[7] = cSymbol;
    buffer[0] |= 0x80;
  }
  else
    buffer[0] &= 0x7f;

  for (int i = 7; i >= 0; i--) {
    ostr.write(&buffer[i], 1);
  }

  if (pLeft != nullptr)
    pLeft->Save(ostr);
  if (pRight != nullptr)
    pRight->Save(ostr);
}

RNode::RNode(istream& input)
{
  unsigned long long data = 0;
  char temp;
  for (int i = 0; i < 8; i++) {
    input.get(temp);
    data >>= 8;
    data |= ((long long)temp << 56);
  }

  cSymbol = (char)data;
  long long mask = 1;
  mask <<= 63;
  if ((data & mask) == (mask))
    fLeaf = true;
  else
    fLeaf = false;
  data <<= 1;
  data >>= 9;
  uFreq = data;

  if (fLeaf) {
    pLeft = nullptr;
    pRight = nullptr;
    return;
  }
  else {
    pLeft = move(huftree_ptr(new RNode(input)));
    pRight = move(huftree_ptr(new RNode(input)));
  }
}

struct RCode {
  long long code;
  unsigned length;
};

class Comp {
public:
  bool operator()(const huftree_ptr& left, const huftree_ptr& right) const
  {
    return *right < *left;
  }
};


array_ptr Frequency(istream& istr)
{
  array_ptr frequency(new array<unsigned long long, 256>);
  frequency->fill(0);
  char x;
  while (istr.get(x)) {
    ++(*frequency)[(unsigned char)x];
  }
  return move(frequency);
}

huftree_ptr MakeTree(array_ptr freqency)
{
  if (freqency->empty())
    return nullptr;
  priority_queue<huftree_ptr, vector<huftree_ptr>, Comp> nodes;
  unsigned time = 0;
  //for_each(freqency->begin(), freqency->end(), [&](pair<char, unsigned> in){nodes.emplace(new RNode(in.first, in.second, true)); });
  for (int i = 0; i < 256; i++) {
    if ((*freqency)[i] != 0)
      nodes.emplace(new RNode(i, (*freqency)[i], true));
  }
  huftree_ptr tempLeft;
  huftree_ptr tempRight;
  huftree_ptr tempRoot;
  while (1) {
    tempLeft = move(nodes.top());
    nodes.pop();
    if (!nodes.empty()) {
      tempRight = move(nodes.top());
      nodes.pop();
      unsigned long long fre = tempLeft->uFreq + tempRight->uFreq;
      tempRoot = move(huftree_ptr(new RNode(0, fre, false, ++time, move(tempLeft), move(tempRight))));
      nodes.push(move(tempRoot));
    }
    else {
      return move(tempLeft);
    }
  }
}

void AddCode(codes_ptr& codes, huftree_ptr& node, vector<bool>& bin)
{
  if (node->fLeaf) {
    (*codes)[(unsigned char)node->cSymbol] = bin;

#ifdef PRINT
    cout << node->cSymbol << "\t";
    for (auto i = bin.begin(); i != bin.end(); ++i) {
      if (*i)
        cout << "1";
      else
        cout << "0";
    }
    cout << endl;
#endif

  }
  if (node->pLeft != nullptr) {
    bin.push_back(false);
    AddCode(codes, node->pLeft, bin);
    bin.pop_back();
  }
  if (node->pRight != nullptr) {
    bin.push_back(true);
    AddCode(codes, node->pRight, bin);
    bin.pop_back();
  }
}

codes_ptr MakeCodes(huftree_ptr tree)
{
  codes_ptr codes(new array<vector<bool>, 256>);
  vector<bool> temp;
  temp.push_back(false);
  AddCode(codes, tree->pLeft, temp);
  temp.pop_back();
  temp.push_back(true);
  AddCode(codes, tree->pRight, temp);
  return move(codes);
}

void Code(codes_ptr codes, istream& in, ostream& out)
{
  char x;
  char cWrite;
  deque<bool> buffer;
  while (in.get(x)) {
    auto it = (*codes)[(unsigned char)x];
    for (auto rit = it.begin(); rit != it.end(); ++rit) {
      buffer.push_back(*rit);
    }
    while (buffer.size() >= 8) {
      cWrite = 0;
      for (int i = 0; i < 8; i++) {
        cWrite >>= 1;
        if (buffer.front()) {
          cWrite |= 0x80;
        }
        else
          cWrite &= 0x7f;
        buffer.pop_front();
      }
      out.write(&cWrite, 1);
    }
  }
  while (buffer.size() < 8)
    buffer.push_back(false);
  cWrite = 0;
  for (int i = 0; i < 8; i++) {
    cWrite >>= 1;
    if (buffer.front()) {
      cWrite |= 0x80;
    }
    else
      cWrite &= 0x7f;
    buffer.pop_front();
  }
  out.write(&cWrite, 1);
}

void Encode(string in, string coded)
{
  ifstream input(in, ifstream::binary);
  auto frequency = Frequency(input);
  auto tree = MakeTree(move(frequency));
  input.clear();
  input.seekg(0, ios::beg);
  ofstream outfile(coded, ofstream::binary);
  char header[8] = { 0x7B, 0x68, 0x75, 0x7C, 0x6D, 0x7D, 0x66, 0x66 };
  outfile.write(header, 8);
  tree->Save(outfile);
  for (int i = 0; i < 8; i++)
    header[i] = 0;
  outfile.write(header, 8);
  codes_ptr codes = MakeCodes(move(tree));
  Code(move(codes), input, outfile);

  outfile.close();
  input.close();
}

void Decode(string coded, string out)
{
  ifstream input(coded, ifstream::binary);
  ofstream output(out, ofstream::binary);
  char x;
  for (int i = 0; i < 8; i++)
    input.get(x);
  huftree_ptr tree(new RNode(input));
  for (int i = 0; i < 8; i++)
    input.get(x);
  unsigned long long written = 0;
  huftree_ptr* current = &tree;
  deque<bool> buffer;
  while (written < tree->uFreq) {
    if (!input.eof()) {
      char x;
      input.get(x);
      for (int i = 0; i < 8; i++) {
        char mask = 0x01;
        if ((x & mask) == mask)
          buffer.push_back(true);
        else
          buffer.push_back(false);
        x >>= 1;
      }
    }
    while (!buffer.empty()) {
      if (buffer.front()) {
        current = &(*current)->pRight;
      }
      else {
        current = &(*current)->pLeft;
      }
      buffer.pop_front();
      if ((*current)->fLeaf) {
        output.write(&(*current)->cSymbol, 1);
        written++;
        current = &tree;
      }
      if (written >= tree->uFreq)
        break;
    }
  }
  
  input.close();
  output.close();
}

int main(int argc, char **argv)
{
  string inputFile = "big.txt";
  string codedFile = "out.txt";
  string decodedFile = "decoded.txt";
  Encode(inputFile, codedFile);
  Decode(codedFile, decodedFile);

  return 0;
}